﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Squares
{
    public class RightTriangle
    {
        /// <summary>
        /// Check that that params can made rightTriangle
        /// </summary>
        /// <param name="a">Cathet a. Should be >= 0</param>
        /// <param name="b">Cathet b. Should be >= 0</param>
        /// <param name="c">Hypotenuse c. Should be >= 0</param>
        /// <returns>true - if realy can</returns>
        public static bool IsRightTriangle(double a, double b, double c)
        {
            return Math.Abs(Math.Sqrt(c * c) - Math.Sqrt(a * a + b * b)) < 1.0e-14;
        }

        /// <summary>
        /// Equtate Square of rightTriangle.
        /// </summary>
        /// <param name="CathetA"> greater or equals than 0 (>=0)</param>
        /// <param name="CathetB">greater or equals than 0 (>=0)</param>
        /// <param name="Hypotenuse">greater or equals than 0 (>=0)</param>
        /// <exception cref="ArgumentOutOfRangeException"></exception>
        /// <exception cref="ArgumentException"></exception>
        /// <returns></returns>
        public static double GetSquare(double CathetA, double CathetB, double Hypotenuse)
        {
            if (CathetA < 0.0) throw new ArgumentOutOfRangeException(nameof(CathetA), CathetA, $"Parameter '{nameof(CathetA)}' = {CathetA}. It should be greater or equals than 0 (>=0)");
            if (CathetB < 0.0) throw new ArgumentOutOfRangeException(nameof(CathetB), CathetB, $"Parameter '{nameof(CathetB)}' = {CathetB}. It should be greater or equals than 0 (>=0)");
            if (Hypotenuse < 0.0) throw new ArgumentOutOfRangeException(nameof(Hypotenuse), Hypotenuse, $"Parameter '{nameof(Hypotenuse)}' = {Hypotenuse}. It should be greater or equals than 0 (>=0)");

            if (!IsRightTriangle(CathetA, CathetB, Hypotenuse)) throw new ArgumentException($"Seems like triangle made with params [{CathetA},{CathetB},{Hypotenuse}] not realy rightTriangle", "CathetA, CathetB, Hypotenuse");

            return 0.5 * CathetA * CathetB; 
        }

    }
}
