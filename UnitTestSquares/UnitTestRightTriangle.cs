﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Squares;
using System.Collections.Generic;

namespace UnitTestSquares
{
    [TestClass]
    public class UnitTestRightTriangle
    {
        [TestMethod]
        public void RightTriangle_GetSquare_AllParamNonNegative()
        {
            #region firstparam
            //arrange
            double a = -1, b = 0, c = 0;

            try
            {
                //act
                var result = RightTriangle.GetSquare(a, b, c);

                //assert
                Assert.Fail("An exception should have been thrown");
            }
            catch (ArgumentOutOfRangeException ae)
            {
                Assert.AreEqual(ae.ActualValue, a);
                Assert.IsFalse(string.IsNullOrWhiteSpace(ae.Message));
            }
            catch (Exception e)
            {
                Assert.Fail($"Unexpected exception of type {e.GetType()} caught: {e.Message}");
            }
            #endregion firstparam

            #region secondparam
            //arrange
            a = 0; b = -1; c = 0;

            try
            {
                //act
                var result = RightTriangle.GetSquare(a, b, c);

                //assert
                Assert.Fail("An exception should have been thrown");
            }
            catch (ArgumentOutOfRangeException ae)
            {
                Assert.AreEqual(ae.ActualValue, b);
                Assert.IsFalse(string.IsNullOrWhiteSpace(ae.Message));
            }
            catch (Exception e)
            {
                Assert.Fail($"Unexpected exception of type {e.GetType()} caught: {e.Message}");
            }
            #endregion secondparam

            #region thirdparam
            //arrange
            a = 0; b = 0; c = -1;

            try
            {
                //act
                var result = RightTriangle.GetSquare(a, b, c);

                //assert
                Assert.Fail("An exception should have been thrown");
            }
            catch (ArgumentOutOfRangeException ae)
            {
                Assert.AreEqual(ae.ActualValue, c);
                Assert.IsFalse(string.IsNullOrWhiteSpace(ae.Message));
            }
            catch (Exception e)
            {
                Assert.Fail($"Unexpected exception of type {e.GetType()} caught: {e.Message}");
            }
            #endregion thirdparam
        }


        [TestMethod]
        public void RightTriangle_IsRightTriangle()
        {
            // arrange

            var allZero = new[] { 0, 0, 0 };
            var notTr = new[] { 1, 2, 3 };
            var isTr = new[] { 3.0, 4.0, 5.0 };
            var notTrSmall = new[] { 0.0000030, 0.000004001, 0.000005 };


            //act //assert
            Assert.IsTrue(RightTriangle.IsRightTriangle(allZero[0], allZero[1], allZero[2]), nameof(allZero));
            Assert.IsFalse(RightTriangle.IsRightTriangle(notTr[0],   notTr[1],  notTr[2]), nameof(notTr));
            Assert.IsTrue(RightTriangle.IsRightTriangle(isTr[0],     isTr[1],   isTr[2]), nameof(isTr));
            Assert.IsFalse(RightTriangle.IsRightTriangle(notTrSmall[0], notTrSmall[1], notTrSmall[2]), nameof(notTrSmall));
        }

        [TestMethod]
        public void RightTriangle_GetSquare()
        {
            // arrange
            var data = new[,] {
                { 0, 0, 0 },
                {3, 4, 5},
                {2, 5, 5.3851648071345},   // 5.3851
            };

            //act
            var result = new List<double>();
            for (int i = 0; i < data.GetLength(1); i++)
                result.Add(RightTriangle.GetSquare(data[i, 0], data[i, 1], data[i, 2]));

            //assert
            Assert.AreEqual(result[0], 0.0, double.Epsilon, "[0,0,0]");
            Assert.AreEqual(result[1], 6.0, double.Epsilon, "[3,4,5]");
            Assert.AreEqual(result[2], 5.0, double.Epsilon, "not realy triangle");
        }
    }
}
